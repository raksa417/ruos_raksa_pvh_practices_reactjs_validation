import React, { Component } from "react";
import { Formik } from 'formik';
import * as Yup from 'yup';

export default class InputValidation2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      person: [
        {
          name: "Raksa",
          phoneNum: "085 344 083",
        },
      ],
      msg: "",
    };
  }

  render() {
    return (
      <Formik
          initialValues= {{
            name: '',
            phoneNum: ''
          }}
          validationSchema= {Yup.object({
            name: Yup.string()
            .matches(/^[a-zA-Z]*$/, "Please input only characters")
              .required("Required"),
            phoneNum: Yup.string()
               .matches(/((^[0-9]{9,10})+$)/,"Please input only numbers & 9-10 digits")
               .required("Required"),
          })}
          onSubmit={(values) => {
            if (values !== "") {
              setTimeout(() => {
                const newPerson = {
                  name: values.name,
                  phoneNum: values.phoneNum,
                };

                this.setState({
                  person: [...this.state.person, newPerson],
                  msg: "Input recorded successfully !",
                  errorInputName: "",
                  errorInputPhoneNum: "",
                });
              }, 400);
            } else {
              this.setState({
                ErrorMessage: "ssss",
              });
            }
          }}
      >
          { formik => (
            <div className="mt-10">
              <div class="flex justify-center">
                <form onSubmit={formik.handleSubmit}>
                  <div>
                    <label
                      htmlFor="name"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Name
                    </label>
                    <input
                      id="name"
                      name="name"
                      type="text"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.name}
                      class="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Kakada"
                    />
                    {formik.touched.name && formik.errors.name ? (
                      <div class="text-red-500">{formik.errors.name}</div>
                    ) : null}
                  </div>

                  <div>
                    <label
                      htmlFor="phoneNum"
                      class="block mt-3 mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Phone number
                    </label>
                    <input
                      id="phoneNum"
                      name="phoneNum"
                      type="text"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.phoneNum}
                      class="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="012 345 6789"
                    />
                    {formik.touched.phoneNum && formik.errors.phoneNum ? (
                      <div class="text-red-500">{formik.errors.phoneNum}</div>
                    ) : null}
                  </div>

                  <p
                    className={
                      this.state.msg === "" ? "hidden" : "text-green-500 mb-5"
                    }
                  >
                    {this.state.msg}
                  </p>

                  <div className="mt-5">
                    <button
                      type="submit"
                      class="w-96 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm  px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              </div>

              {/* Display in table */}
              <h1 className="text-xl text-center font-bold  mt-12 mb-3">
                Display Person Information
              </h1>
              <div class="flex justify-center">
                <br />
                <table class="text-sm text-gray-500 dark:text-gray-400 text-center  w-96">
                  <thead class="text-[16px] text-white uppercase bg-blue-500 dark:bg-gray-700 dark:text-gray-400">
                    <tr className="">
                      <th class="px-6 py-3 rounded-l-lg">Name</th>
                      <th class="rounded-r-lg">Phone Number</th>
                    </tr>
                  </thead>
                  <tbody className="text-[15px]">
                    {this.state.person.map((obj) => (
                      <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                          {obj.name}
                        </td>
                        <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                          {obj.phoneNum}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          )}
     </Formik>
    );
}}
