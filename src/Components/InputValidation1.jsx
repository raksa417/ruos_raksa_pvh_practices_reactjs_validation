import React, { Component } from "react";
import "flowbite";

export default class InputValidation1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      person: [
        {
          name: "Raksa",
          phoneNum: "085 344 083",
        },
      ],
      newName: "",
      newPhoneNum: "",
      errorInputName: "",
      errorInputPhoneNum: "",
      errorFieldRequired: "",
      msg: "",
    };
  }

  handleName = (e) => {
    if (e.target.value.match(/^[ a-zA-Z ]+$/)) {
      this.setState({
        newName: e.target.value,
        errorInputName: "",
      });
    } else if (!e.target.value.match(/^[ a-zA-Z ]+$/)) {
      this.setState({
        errorInputName: "Please input only characters",
      });
    }

    if (e.target.value === "") {
      this.setState({
        errorInputName: "Name is required !",
      });
    }
  };

  handlePhoneNum = (e) => {
    if (e.target.value.match(/\b[0-9]{9,10}\b/)) {
      this.setState({
        newPhoneNum: e.target.value,
        errorInputPhoneNum: "",
      });
    } else if (!e.target.value.match(/\b[0-9]{9,10}\b/)) {
      this.setState({
        errorInputPhoneNum: "Input must between 9 to 10  digits and  only numeric ",
      });
    }
    if (e.target.value === "") {
      this.setState({
        errorInputPhoneNum: "Phone Number is required !",
      });
    }
  };

  submitPerson = (e) => {
    e.preventDefault();

    if (this.state.newPhoneNum !== "" && this.state.newPhoneNum !== "") {
      const newPerson = {
        name: this.state.newName,
        phoneNum: this.state.newPhoneNum,
      };

      this.setState({
        person: [...this.state.person, newPerson],
        msg: "Input recorded successfully !",
        errorInputName: "",
        errorInputPhoneNum: "",
      });
    } else {
      this.setState({
        errorFieldRequired: "All input fields are required",
        errorInputName: "",
        errorInputPhoneNum: "",
        msg: "",
      });
    }
  };

  render() {
    return (
      <div className="mt-10">
        <div class="flex justify-center">
          <form>
            <div>
              <label
                for="name"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Name
              </label>
              <input
                type="text"
                id="name"
                onChange={this.handleName}
                onBlur={this.handleName}
                class="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Kakada"
              />
              <p
                className={
                  this.state.errorInputName === ""
                    ? "hidden"
                    : "text-red-700 mb-2"
                }
              >
                {this.state.errorInputName}
              </p>
            </div>

            <div>
              <label
                for="phone_num"
                class="block mt-3 mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Phone number
              </label>
              <input
                type="text"
                id="phone_num"
                onChange={this.handlePhoneNum}
                onBlur={this.handlePhoneNum}
                class="w-96 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="012 345 6789"
              />
              <p
                className={
                  this.state.errorInputPhoneNum === ""
                    ? "hidden"
                    : "text-red-700 mb-2"
                }
              >
                {this.state.errorInputPhoneNum}
              </p>
            </div>

            <p
              className={
                this.state.errorFieldRequired === ""
                  ? "hidden"
                  : "text-red-700 mb-2"
              }
            >
              {this.state.errorFieldRequired}
            </p>
            <p
              className={
                this.state.msg === "" ? "hidden" : "text-green-500 mb-5"
              }
            >
              {this.state.msg}
            </p>

            <div className="mt-5">
              <button
                type="submit"
                onClick={this.submitPerson}
                class="w-96 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm  px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Submit
              </button>
            </div>
          </form>
        </div>

        {/* Display in table */}
        <h1 className="text-xl text-center font-bold  mt-12 mb-3">
          Display Person Information
        </h1>
        <div class="flex justify-center">
          <br />
          <table class="text-sm text-gray-500 dark:text-gray-400 text-center  w-96">
            <thead class="text-[16px] text-white uppercase bg-blue-500 dark:bg-gray-700 dark:text-gray-400">
              <tr className="">
                <th class="px-6 py-3 rounded-l-lg">Name</th>
                <th class="rounded-r-lg">Phone Number</th>
              </tr>
            </thead>
            <tbody className="text-[15px]">
              {this.state.person.map((obj) => (
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                  <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {obj.name}
                  </td>
                  <td class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {obj.phoneNum}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
